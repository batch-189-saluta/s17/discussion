/*
	you can declare functions by using:
	1.function keyword
	2.function name
	3.open/close parenthesis
	4.open/close curly braces
*/
function sayHello (){
	console.log('Hello there!')
}
/*
	you can invoke a funtion by calling its function name and including
	the parenthesis
*/
sayHello();


/*
	you can assign a function to a variable. 
	the function name would not be required
*/
let sayGoodbye = function(){
	console.log('Goodbye!')
}
/*
	you can invoke a function inside a variable by using the variable name
*/
sayGoodbye()


// you can also re-assign a function as a new value of a variable
sayGoodbye = function(){
	console.log('Au Revoir!')
}
sayGoodbye()

/*
	declaring a constant variable with a function as a value will not
	allow that function to be changed or reassigned
*/
const sayHelloInJapanese = function(){
	console.log('Ohayo!')
}

sayHelloInJapanese()


let action = 'Run'
/*
	GLOBAL SCOPE - You can use a variable inside a function if the 
	variable is declared outside of it
*/
function doSomethingRandom (){
	console.log(action)
}

doSomethingRandom()

/*
	LOCAL/FUNCTION SCOPE - you CANNOT use a variable outside of a function
	if it is within the function scope/curly braces
*/
/*function doSomethingRandom (){
	let action = 'Run'
}

console.log(action)*/

/*
You can nest functions inside of a function as long as you 
invoke the child function within the scope of the parent function
*/
function viewProduct(){

	console.log('Viewing a Product')
	function addToCart() {
		console.log('Added product to cart')
	}
	addToCart()
}
viewProduct()

// BUILT-IN JAVASCRIPT FUNCTIONS
/*
	alert function is a built-in javascript function where we can show
	alerts to the user
*/
function singASong(){
	alert('La la la')
}
singASong()

/*
 	any statement like 'console.log' will run only after the alert has been
 	closed if it is invoked below of the alert function
*/
console.log('clap clap clap')

/*
	PROMPT is a built-in javascript function that we can 
	use to take input from the user
*/
function enterUserName(){
	let userName = prompt('Enter your username')
	console.log(userName)
}
enterUserName()

/*
	you can use a prompt outside of a function. make sure to 
	output the prompt value by assigning to a variable and using
	console.log
*/
let userAge = prompt('enter your Age')
console.log(userAge)
/*console.log(prompt('enter your Age'))*/

